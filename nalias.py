#!/usr/bin/env python3

import sys
import os
import argparse
from typing import Dict, Optional
import json
import re
from pathlib import Path

regex_name = r'([A-Za-z][A-Za-z0-9_-]*)'
regex_command = r"'([A-Za-z0-9\\\/~.&\"_\-\| > ]+)'"
regex_pattern = r"alias " + regex_name + r"=" + regex_command
regex = re.compile(regex_pattern)


class DuplicateAliasException(Exception):
    pass


class AliasStore:
    def __init__(self, config_json: Dict[str, str]):
        self.aliases_path = Path(config_json["aliases_path"]).expanduser()
        self.config = config_json
        self.aliases = {}
        Path(self.aliases_path).touch(exist_ok=True)
        with open(self.aliases_path, 'r+') as file:
            for name, command in regex.findall(file.read()):
                self.aliases[name] = command

    def add(self, name: str, command: str):
        if name in self.aliases: raise DuplicateAliasException
        self.aliases[name] = command

    def get(self, name: str) -> str:
        return self.aliases[name]

    def remove(self, name: str):
        del self.aliases[name]

    def commit(self) -> None:
        result = ''.join(f"alias {name}='{cmd.rstrip()}'\n" for name, cmd in self.aliases.items())

        with open(self.aliases_path, 'w+') as file:
            file.write(result)

    def has_duplicates(self, name: str) -> Optional[str]:
        if name in self.aliases:
            return self.aliases[name]


def parse_args(args):
    parser = argparse.ArgumentParser()

    parser.add_argument("-n", "--name")
    parser.add_argument("-c", "--command")
    parser.add_argument("-r", "--remove", metavar="NAME")
    parser.add_argument(
        '-i', "--init",
        action="store_true",
        help="Initialize config.json with content of config.default.json"
    )

    ns = parser.parse_args(args)
    if ns.remove and (ns.name or ns.command):
        parser.print_help()
        exit(1)
    return ns.name, ns.command, ns.remove, ns.init


def path_of_this_file() -> Path:
    return Path(__file__).parent.absolute()


def init_config():
    with open(path_of_this_file().joinpath("config.json"), 'w+') as file:
        with open(path_of_this_file().joinpath("config.default.json"), 'r') as default:
            file.write(default.read())
    print("Initialized config.")


def get_config() -> Dict[str, str]:
    dir_path = path_of_this_file()
    path = dir_path.joinpath('config.json' if dir_path.joinpath('config.json').exists() else 'config.default.json')
    with open(dir_path.joinpath(path), 'r') as file:
        return json.load(file)


if __name__ == "__main__":
    name, command, remove_name = [''] * 3

    if len(sys.argv) > 1:
        name, command, remove_name, init = parse_args(sys.argv[1:])
        if init:
            init_config()
            exit()

    while not remove_name and not name:
        name = input("Enter alias name:\n")

    while not remove_name and not command:
        command = input("Enter command:\n")

    # get config from file
    config = get_config()

    alias_store = AliasStore(config)

    if remove_name:
        try:
            command = alias_store.get(remove_name)
        except KeyError:
            print(f"Alias {remove_name} does not exist.")
            exit(1)
        if input(f"Remove alias '{remove_name}' with command '{command}'? [Y/n] ").lower() in ["n"]:
            exit()
        alias_store.remove(remove_name)
    else:
        try:
            alias_store.add(name, command)
        except DuplicateAliasException:
            duplicate = alias_store.has_duplicates(name)
            answer = input(
                f"Alias with name '{name}' already exists."
                f"Command is: '{duplicate}' Replace? [y/N] "
            )
            if answer.lower() not in ["yes", "y", "yay"]:
                exit()
            # remove old alias and add new one
            alias_store.remove(name)
            alias_store.add(name, command)

    alias_store.commit()

    print(f"Alias '{name}' with command '{command}' {'removed' if remove_name else 'added'}.")
    # Restart shell, makes alias immediately usable
    os.system(config["shell"])
