nalias
======

A simple alias manager.

Usage
-----

.. code-block::

    usage: nalias.py [-h] [-n NAME] [-c COMMAND] [-r NAME] [-i]

    optional arguments:
    -h, --help            show this help message and exit
    -n NAME, --name NAME
    -c COMMAND, --command COMMAND
    -r NAME, --remove NAME
    -i, --init            Initialize config.json with content of config.default.json
