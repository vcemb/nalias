import setuptools

with open('README.rst', 'r') as file:
    long_description = file.read()

setuptools.setup(
    name="nalias",
    version="0.0.2",
    author="Victor Embacher",
    author_email="victor.emabacher@gmail.com",
    description="A simple alias manager.",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://gitlab.com/vcemb/nalias",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: *nix",
    ],
    python_requires='>=3.8',
)
